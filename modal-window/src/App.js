import React, {Component} from 'react';
import './App.scss';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal"

class App extends Component {

    state = {
        isOpen: false,
        isOpenAlt: false,
    }


    openModal = () => {
        this.setState({isOpen: true})
    }

    openModalAlt = () => {
        this.setState({isOpenAlt: true})
    }

    closeModal = () => {
        this.setState({isOpen: false})
    }

    closeModalAlt = () => {
        this.setState({isOpenAlt: false})
    }


    render() {

        return (
            <div className='App'>
                <Button text='Open first modal' bgColor='green' onClick={this.openModal}/>
                <Button text='Open second modal' bgColor='blue' onClick={this.openModalAlt}/>

                {this.state.isOpen && <Modal
                    header='Do you want to delete this file?'
                    text='Once you delete this file, it won’t be possible to undo this action.
Are you sure you want to delete it?'
                    cross={true}
                    onClick={this.closeModal}
                    actions={[
                        <Button text='OK' className='modalButtons' />,
                        <Button text='Cancel' className='modalButtons' onClick={this.closeModal} />
                    ]}
                />}

                {this.state.isOpenAlt && <Modal
                    header='Another header'
                    text='Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias animi consequatur culpa et illum minus odit pariatur quis recusandae, veniam?'
                    cross={true}
                    onClick={this.closeModalAlt}
                    actions={[
                        <Button text='Submit' className='modalButtons'/>,
                        <Button text='Exit' className='modalButtons' onClick={this.closeModalAlt}/>
                    ]}
                />}

            </div>
        );
    }
}


export default App;
