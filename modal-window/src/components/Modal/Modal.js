import React, {Component} from 'react';
import './Modal.scss'

class Modal extends Component {

    render() {

        const {header, text, onClick, actions, cross} = this.props

        return (
            <div>
                <div className='modal-overlay' onClick={onClick}/>

                <div className="modal">
                    <h3 className='modal__header'>{header}</h3>
                    <span>{text}</span>
                    {cross && <button className='crossBtn' onClick={onClick}>X</button>}
                    <div>
                        {actions}
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;