import React, {Component} from 'react';
import './Button.scss'

class Button extends Component {
    render() {

        const {text, bgColor, className, onClick} = this.props

        return (
            <button
                style={{backgroundColor: bgColor}}
                onClick={onClick}
                className={className}
            >
                {text}
            </button>
        );
    }
}

export default Button;